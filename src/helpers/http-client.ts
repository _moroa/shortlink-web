import axios, {AxiosError, AxiosRequestHeaders} from "axios";

export const JSONRequestHeaders: AxiosRequestHeaders = {
    "Content-Type": "application/json",
    Accept: "application/json",
};


// Http Request Default Configurations
axios.defaults.baseURL = process.env.REACT_APP_API_URL;
axios.defaults.headers.common = {
    ...JSONRequestHeaders,
};

export const HttpClientErrorHandler = (error: AxiosError): string => {
    const DEFAULT_ERROR_MESSAGE ="An error has occurred and we're working to fix the problem!\n" +
        "We'll be up and running shortly."
    const {response, request } = error
    if (response) {
        const {status, data} = response
        switch (status) {
            case 400:
            case 422:
                return data.message
            case 401:
                return "Sorry, Your Session Key has expired, please use the login function\n" +
                    "              again to gain access."
            case 403:
                return " Sorry, you're not permitted to access this service. If you feel this is a mistake, contact admin."
            default:
                return DEFAULT_ERROR_MESSAGE
        }
    } else if(request){
        return error.request.response;
    } else{
        return DEFAULT_ERROR_MESSAGE
    }
}