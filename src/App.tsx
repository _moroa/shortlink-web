import React from 'react';
import {QueryClient, QueryClientProvider} from "react-query";
import {ReactQueryDevtools} from "react-query/devtools";
import {NotificationsProvider} from '@mantine/notifications';
import UrlShortenerPage from "./features/shorturls/page/url-shortener-page";


const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            refetchOnWindowFocus: false,
        },
    },
});

const App = (): JSX.Element => (
    <QueryClientProvider client={queryClient}>
        <NotificationsProvider autoClose={7000} position="top-right">
            <UrlShortenerPage/>
        </NotificationsProvider>
        <ReactQueryDevtools initialIsOpen={false}/>
    </QueryClientProvider>
)


export default App;
