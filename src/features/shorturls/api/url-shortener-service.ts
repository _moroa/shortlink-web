import axios, {AxiosError} from "axios";
import {useMutation} from "react-query";
import UrlShortenerRequest from "./request/url-shortener-request";
import UrlShortener from "./model/url-shortener";

const UrlShortenerService = async (
    req: UrlShortenerRequest
): Promise<UrlShortener> => {
    const {data} = await axios.post("/url", req)
    return data
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const useUrlShortenerService = () => useMutation<UrlShortener, AxiosError, UrlShortenerRequest>(UrlShortenerService, {
    mutationKey: "UrlShortenerService"
})

export default useUrlShortenerService