import UrlShortenerMetrics from "./url-shortener-metrics";

export default interface UrlShortener {
    id: string
    shortenUrl: string
    metrics?: UrlShortenerMetrics[]
}