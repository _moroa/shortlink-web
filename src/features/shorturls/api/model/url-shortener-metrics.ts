export default interface UrlShortenerMetrics {
    id: string
    agent: string
    ip: string
    type: string
}