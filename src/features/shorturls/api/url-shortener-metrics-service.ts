import axios, {AxiosError} from "axios";
import {useQuery} from "react-query";
import UrlShortener from "./model/url-shortener";

const UrlShortenerMetricsService = async (
    req: string
): Promise<UrlShortener> => {
    const {data} = await axios.get(`/url/metrics/${req}`)
    return data
}

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
const useUrlShortenerMetricsService = (id: string) =>
    useQuery<Promise<UrlShortener>, AxiosError, UrlShortener>(
        ["GetUrlShortenerMetrics", id],
    () => UrlShortenerMetricsService(id)
)

export default useUrlShortenerMetricsService