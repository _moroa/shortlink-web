import * as yup from "yup";

export default yup.object().shape({
    url: yup
        .string()
        .url("Invalid URL")
        .required("URL is required.")
})