import React, {FormEvent, useEffect, useState} from "react";
import {useFormik} from "formik";
import {ActionIcon, Alert, Center, Space, TextInput, Title} from "@mantine/core";
import {MdOutlineCancel} from "react-icons/md";
import {CgWebsite} from "react-icons/cg";
import {AiOutlineCheckCircle, AiOutlineSend} from "react-icons/ai";
import {useNotifications} from "@mantine/notifications";
import useUrlShortenerService from "../api/url-shortener-service";
import ShortUrlSchema from "../api/validator/url-shortener-schema";
import {HttpClientErrorHandler} from "../../../helpers/http-client";
import UrlShortenerDetailsCard from "./url-shortener-details-card";

const UrlShortenerForm: React.FC = () => {
    const initialValues = {
        url: ""
    }
    Object.freeze(initialValues) // values can no longer be changed
    const notifications = useNotifications();
    const [open, setOpen] = useState(false)

    const {
        isSuccess,
        isError,
        isLoading,
        mutate: urlShortener,
        error,
        data,
    } = useUrlShortenerService()

    useEffect(() => {
        if (isSuccess && data) {
            notifications.showNotification({
                color: 'teal',
                icon: <AiOutlineCheckCircle size={32} />,
                title: "URL Shortener",
                message: "URL shortener generate completed!",
                autoClose: true,
            })
            setOpen(true)
        }
    }, [isSuccess, data]);

    const form = useFormik({
        initialValues,
        validationSchema: ShortUrlSchema,
        onSubmit: ({url}) => urlShortener({
            originalURL: url
        })
    });


    const onFormSubmit = async (
        event: FormEvent<HTMLFormElement>
    ): Promise<void> => {
        event.preventDefault();
        await form.submitForm();
    };

    return (
        <>
            {error && isError && (
                <Alert
                    icon={<MdOutlineCancel size={16}/>}
                    title="URL shortener Failed!"
                    color="red"
                >
                    {HttpClientErrorHandler(error)}
                </Alert>
            )}

            {
                !(open && data) ? (
                    <form onSubmit={onFormSubmit}>
                        <Title order={4}>Enter the URL to be shortened</Title>
                        <Space h="xl" />
                        <div style={{display: 'flex', paddingTop: 5, width: "100%"}}>
                            <TextInput
                                id="url"
                                name="url"
                                icon={<CgWebsite/>}
                                placeholder="Enter your url"
                                onChange={form.handleChange}
                                error={form.errors.url}
                            />
                            <ActionIcon
                                type="submit"
                                size={36}
                                color="blue"
                                variant="filled"
                                loading={isLoading}
                            >
                                <AiOutlineSend/>
                            </ActionIcon>
                        </div>
                    </form>
                ) : (
                    <Center>
                        <UrlShortenerDetailsCard
                            onClose={setOpen}
                            id={data.id}
                            originalURL={form.values.url}
                            shortenUrl={data.shortenUrl}
                        />
                    </Center>
                )
            }
        </>
    );
}

export default UrlShortenerForm
