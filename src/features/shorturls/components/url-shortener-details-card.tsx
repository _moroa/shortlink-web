import React, {useState} from "react";
import {ActionIcon, Anchor, Button, Card, Group, Space, Text, TextInput, Title, UnstyledButton} from "@mantine/core";
import {CgWebsite} from "react-icons/cg";
import {BsClipboard, BsClipboardCheck} from "react-icons/bs";
import {useClipboard} from "@mantine/hooks";
import {IoArrowBackCircleOutline} from "react-icons/io5";
import UrlShortenerRequest from "../api/request/url-shortener-request";
import UrlShortener from "../api/model/url-shortener";
import UrlShortenerMetricsDrawer from "./url-shortener-metrics-drawer";


interface UrlShortenerDetailsCardProps extends UrlShortener, UrlShortenerRequest {
    onClose: React.Dispatch<React.SetStateAction<boolean>>,
}

const UrlShortenerDetailsCard: React.FC<UrlShortenerDetailsCardProps> = ({
                                                                             onClose,
                                                                             id,
                                                                             shortenUrl,
                                                                             originalURL
                                                                         }) => {
    const clipboard = useClipboard({timeout: 500});
    const [openDrawer, setOpenDrawer] = useState(false)
    return <>
        {openDrawer && <UrlShortenerMetricsDrawer id={id}
                                                  isOpen={openDrawer}
                                                  onClose={setOpenDrawer}
                                                  shortenUrl={shortenUrl}/>}


        <Card padding="xl">
            <Card.Section>
                <Space m={80}/>
                <UnstyledButton onClick={() => onClose(false)}>
                    <Group>
                        <IoArrowBackCircleOutline size={40}/>
                        <div>
                            <Text variant="link">Create other shortened URL.</Text>
                        </div>
                    </Group>
                </UnstyledButton>
                <Space h="xl"/>
                <Title order={4}>Your shortened URL</Title>
                <Space h="xl"/>
                <Text size="md" style={{color: "grey", lineHeight: 1.5}}>
                    Copy the shortened link and share it in messages, texts, posts, websites and other locations.
                </Text>
                <Space h="xl"/>
                <div style={{display: 'flex', paddingTop: 5}}>
                    <TextInput
                        id="url"
                        value={shortenUrl}
                        defaultValue={shortenUrl}
                        icon={<CgWebsite/>}
                        disabled

                    />
                    <ActionIcon
                        type="submit"
                        size={36}
                        color={clipboard.copied ? 'teal' : 'blue'}
                        variant="filled"
                        onClick={() => clipboard.copy(shortenUrl)}
                    >
                        {clipboard.copied ? <BsClipboardCheck/> : <BsClipboard/>}
                    </ActionIcon>
                </div>
            </Card.Section>
            <Space h="xl"/>

            <Card.Section>
            <span>
                Long URL: <Anchor href={originalURL} target="_blank">{originalURL}</Anchor>
            </span>
            </Card.Section>
            <Space h="md"/>
            <Card.Section>
                <Text size="md" style={{color: "grey", lineHeight: 1.5}}>
                    Track <Button onClick={() => setOpenDrawer(true)} type="button" compact variant="subtle">the total of clicks</Button>from your shortened URL.
                </Text>
                <Space m={32}/>
            </Card.Section>

        </Card>
    </>
}
export default UrlShortenerDetailsCard