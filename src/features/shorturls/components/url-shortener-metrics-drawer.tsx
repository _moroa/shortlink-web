import React from "react";
import {Alert, Card, Drawer, LoadingOverlay, Space, Table, Text} from "@mantine/core";
import {MdOutlineCancel} from "react-icons/md";
import UrlShortener from "../api/model/url-shortener";
import useUrlShortenerMetricsService from "../api/url-shortener-metrics-service";
import {HttpClientErrorHandler} from "../../../helpers/http-client";


interface UrlShortenerMetricsDrawerProps extends UrlShortener{
    isOpen: boolean,
    onClose:  React.Dispatch<React.SetStateAction<boolean>>
}
const UrlShortenerMetricsDrawer: React.FC<UrlShortenerMetricsDrawerProps> = ({id,isOpen, onClose}) => {

        const {
            isSuccess,
            isError,
            isLoading,
            error,
            data,
        } = useUrlShortenerMetricsService(id)

    const ths = (
        <tr>
            <th>IP</th>
            <th>Agent</th>
            <th>Event</th>
        </tr>
    );

    return (
        <>
            <LoadingOverlay visible={isLoading} />
            <Drawer
                opened={isOpen}
                onClose={() => onClose(false)}
                title="Total URL Clicks"
                padding="xl"
                position="right"
                size="60%"
            >

                <Card padding="xl">

                    {error && isError && (
                        <Alert
                            icon={<MdOutlineCancel size={16}/>}
                            title="Error!"
                            color="red"
                        >
                            {HttpClientErrorHandler(error)}
                        </Alert>
                    )}

                    {
                        isSuccess && data && <Card.Section>
                                <Text weight={500} size="lg">
                                    The total number of clicks that your link has received so far.
                                </Text>
                                <Space />
                                <Text style={{
                                    display: "inline-block",
                                    padding: "15px 20px",
                                    borderRadius: "3px",
                                    font: "55px arial",
                                    color: "#444"
                                }}> {data.metrics?.length} </Text>
                                <Space />


                                <Table striped highlightOnHover verticalSpacing="md">
                                    <thead>{ths}</thead>
                                    <tbody>{ data.metrics?.map((element) => (
                                        <tr key={element.id}>
                                            <td>{element.ip}</td>
                                            <td>{element.agent}</td>
                                            <td>{element.type}</td>
                                        </tr>
                                    )) }</tbody>
                                </Table>
                            </Card.Section>
                    }
                </Card>
            </Drawer>
        </>
    )
}

export default UrlShortenerMetricsDrawer