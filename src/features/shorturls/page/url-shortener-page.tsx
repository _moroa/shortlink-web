import {Center, Container, Group} from "@mantine/core";
import React from "react";
import UrlShortenerForm from "../components/url-shortener-form";

const UrlShortenerPage: React.FC = () =>(
    <Container style={{ width: "100%", height: "100vh"}}>
       <Center style={{ padding:"40px 20px 25px"}}><h1>Short Links</h1></Center>
        <Center >
            <Group direction="column">

                <UrlShortenerForm />
            </Group>
        </Center>
    </Container>
)

export default UrlShortenerPage