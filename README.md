# Getting Started with Create React App

This project has been generated using the `create-react-app` typescript template from the 

For detailed instructions, please refer to the [documentation](https://github.com/facebook/create-react-app).


##Install Dependencies

###Using NPM
- Run npm i to install the project dependencies
### Using Yarn
- Run yarn to install the project dependencies

## Available Scripts
In the project directory, you can run:
### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Project features

### Project structure

The project code base is mainly located within the `src` folder. This folder is divided in:

- `features` - containing code base and pages for the application
- `helpers` - containing shared code base between components

```
.
├── src
│   ├── features               # App functionality or module source code folder
│   │   ├── shorturl
│   │       ├── components      # independent and reusable bits of UI
│   │       ├── pages           # website page
│   │       └── api             # API services
                 ├── request    # represents API request payload
│   │            ├── validator  # represents API request payload constraint
│   │            └── model      # represents API response
│   │   
│   │
│   ├── helpers                 # Shared code
│       └── http-client.ts      # Http client specific helpers
│  
│       
└── package.json        # Webpack configuration
```
